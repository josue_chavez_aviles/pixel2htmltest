'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');

gulp.task('vendors', function() {
    gulp.src([
        'bower_components/font-awesome/fonts/**',
        'bower_components/ionic/fonts/**'
    ]).pipe(gulp.dest('dist/fonts/'));
});