'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('scripts', [], function() {
    gulp.src([
            'node_modules/jquery/dist/jquery.js',
            'node_modules/bootstrap/js/collapse.js',
            'src/scripts/**/*.js'
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('main.min.js'))
        .pipe(uglify({
            mangle: false,
            outSourceMap: true
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist/javascript/'));
});