'use strict';

var gulp = require('gulp');

gulp.task('assets', function() {
    gulp.src(['src/assets/**/*.*', 'package.json'])
        .pipe(gulp.dest('./dist/images/'));
});

gulp.task('fonts', function() {
    gulp.src(['node_modules/roboto-fontface/fonts/**/*.*','package.json'])
        .pipe(gulp.dest('./dist/fonts/'));
});