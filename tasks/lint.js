'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    eslint = require('gulp-eslint'),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('lint', function() {
    gulp.src([
            'src/scripts/**/*.js'
        ])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.results(results => {
                // Called once for all ESLint results.
                console.log(`Total Results: ${results.length}`);
    console.log(`Total Warnings: ${results.warningCount}`);
    console.log(`Total Errors: ${results.errorCount}`);
}));
});