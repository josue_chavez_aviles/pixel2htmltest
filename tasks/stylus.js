'use strict';
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    stylus = require('gulp-stylus'),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('stylus', function() {
    gulp.src
    ([
        'node_modules/roboto-fontface/css/roboto/roboto-fontface.css',
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'src/stylus/*.styl'
    ])
    //.pipe(sourcemaps.init())
    .pipe(stylus({
        compress: true
    }))
    .pipe(concat('main.min.css'))
    //.pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/stylesheets'));
});