var gulp = require('gulp'),
    pug = require( 'gulp-pug' );

gulp.task( 'pug', function (done) {
    gulp.src( 'src/*.pug' )
        .pipe( pug() )
        .pipe( gulp.dest( './dist/' ) )
        .on( 'end', done );
} );